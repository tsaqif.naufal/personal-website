from django import forms
from datetime import date

class ScheduleForm(forms.Form):
	text_attrs = {
		'size': 40,
	}

	date = forms.DateField(label = "Date", widget = forms.DateInput(attrs = {'type': 'date', 'min': date.today()}))
	time = forms.TimeField(label = "Time", widget = forms.TimeInput(attrs = {'type': 'time'}))
	category = forms.CharField(label = "Category", max_length = 20, widget = forms.TextInput(attrs = text_attrs))
	activity_name = forms.CharField(label = "Activity Name", max_length = 50, widget = forms.TextInput(attrs = text_attrs))
	place = forms.CharField(label = "Place", max_length = 30, widget = forms.TextInput(attrs = text_attrs))
