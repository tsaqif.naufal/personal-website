from django.db import models

class Schedule(models.Model):
	category = models.CharField(max_length = 20)
	activity_name = models.CharField(max_length = 50)
	date = models.DateField()
	time = models.TimeField()
	place = models.CharField(max_length = 30)

	def __str__(self):
		return f"{self.category} - {self.activity_name}"