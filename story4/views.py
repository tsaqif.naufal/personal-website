from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .forms import ScheduleForm
from .models import Schedule

def home(request):
	return render(request, 'home.html')

def profile(request):
	return render(request, 'profile.html')

def educations(request):
	return render(request, 'educations.html')

def experiences(request):
	return render(request, 'experiences.html')

def skills(request):
	return render(request, 'skills.html')

def schedule(request):
	form = ScheduleForm()
	schedule = Schedule.objects.all()
	return render(request, 'schedule.html', {'form': form, 'schedule': schedule})

def add_schedule(request):
	if request.method == 'POST':
		form = ScheduleForm(request.POST)
		if form.is_valid():
			date = form.cleaned_data['date']
			time = form.cleaned_data['time']
			category = form.cleaned_data['category']
			activity_name = form.cleaned_data['activity_name']
			place = form.cleaned_data['place']
			schedule = Schedule(date = date, time = time, category = category, activity_name = activity_name, place = place)
			schedule.save()
			return HttpResponseRedirect(reverse('schedule'))
	else:
		form = ScheduleForm()
		schedule = Schedule.objects.all()

	return render(request, 'schedule.html', {'form': form, 'schedule': schedule})

def del_schedule(request, activity_id):
	try:
		Schedule.objects.get(id=activity_id).delete()
	except Schedule.DoesNotExist:
		return render(request, 'error.html', {'message': f'No activity with ID {activity_id}'})
	return HttpResponseRedirect(reverse('schedule'))

def article(request):
	return render(request, 'article.html')
