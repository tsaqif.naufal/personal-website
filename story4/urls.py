from django.urls import path
from . import views

urlpatterns = [
	path('', views.home, name='home'),
	path('profile', views.profile, name='profile'),
	path('educations', views.educations, name='educations'),
	path('experiences', views.experiences, name='experiences'),
	path('skills', views.skills, name='skills'),
	path('schedule', views.schedule, name='schedule'),
	path('add_schedule', views.add_schedule, name='add_schedule'),
	path('del_schedule/<int:activity_id>', views.del_schedule, name='del_schedule'),
	path('article', views.article, name='article'),
]